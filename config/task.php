<?php
//swoole.task配置文件
return [
    'key' => 'cmd',
    'alias' => [
        'order' => [
            'class' => \app\job\Task::class,
            'methods' => [
                'func1',
                'func2',
            ],
            'finish' => true
        ]
    ],
];